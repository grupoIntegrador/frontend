import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { AtividadeResumed } from '../models/atividadeResumed';
import { Modulo } from '../models/modulo';


@Injectable({
  providedIn: 'root'
})
export class ModuloService {

  private PATH = '/modulo';
  constructor(private httpClient: HttpClient) { }

  public findAll(): Observable<Modulo[]> {
    return this.httpClient.get<Modulo[]>(environment.HOST + this.PATH);
  }

  public save(modulo: Modulo): Observable<Modulo> {
    return this.httpClient.post<Modulo>(environment.HOST + this.PATH, modulo);
  }
}
