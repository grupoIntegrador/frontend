import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { AtividadeResumed } from '../models/atividadeResumed';
import { Atividade } from '../models/atividade';


@Injectable({
  providedIn: 'root'
})
export class AtividadeService {

  private PATH = '/atividade';

  constructor(private httpClient: HttpClient) { }

  public findAllResumed(): Observable<AtividadeResumed[]> {
    return this.httpClient.get<AtividadeResumed[]>(environment.HOST + this.PATH + '/findallresumed');
  }

  public entregues(): Observable<AtividadeResumed[]> {
    return this.httpClient.get<AtividadeResumed[]>(environment.HOST + this.PATH + '/entregues');
  }

  public findAtividade(id: string): Observable<Atividade> {
    return this.httpClient.get<Atividade>(environment.HOST + this.PATH + '/' + id);
  }

  public save(atividade: Atividade): Observable<Atividade> {
    return this.httpClient.post<Atividade>(environment.HOST + this.PATH, atividade);
  }

  public update(atividade: Atividade): Observable<Atividade> {
    return this.httpClient.put<Atividade>(environment.HOST + this.PATH, atividade);
  }

  public enviarProducao(idAtividade: string): Observable<Atividade> {
    return this.httpClient.put<Atividade>(environment.HOST + this.PATH + '/enviar-producao/' + idAtividade, null);
  }


}
