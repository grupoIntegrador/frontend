import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {  HttpClient, HttpHeaders } from '@angular/common/http';
import { Sistema } from '../models/sistema';
import { environment } from '../../environments/environment';



@Injectable({
    providedIn: 'root',
})
export class SistemaService {

    private PATH = '/sistema';

    constructor(private httpClient: HttpClient) {
    }

    public findAll(): Observable<any> {
        return this.httpClient.get(environment.HOST + this.PATH);
    }

    public save( sistema: Sistema): Observable<any> {
        console.log(sistema);
        return this.httpClient.post(environment.HOST + this.PATH, sistema);
    }

}
