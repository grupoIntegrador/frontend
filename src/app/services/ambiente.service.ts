import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Ambiente } from '../models/ambiente';
import { environment } from '../../environments/environment';
import { EntregaAmbiente } from '../models/entrega-ambiente';



@Injectable({
    providedIn: 'root',
})
export class AmbienteService {

    private PATH = '/ambiente';
    private PATH_ENTREGA = '/entrega-ambiente';


    constructor(private httpClient: HttpClient) {
    }

    public findAll(): Observable<Ambiente[]> {
        return this.httpClient.get<Ambiente[]>(environment.HOST + this.PATH);
    }

    public entrega(entrega: EntregaAmbiente): Observable<EntregaAmbiente> {
        return this.httpClient.post<EntregaAmbiente>(environment.HOST + this.PATH_ENTREGA, entrega);
    }

    public retirarAmbiente(retirada: EntregaAmbiente): Observable<any> {
        return this.httpClient.put<EntregaAmbiente>(environment.HOST + this.PATH_ENTREGA, retirada);
    }

    public findAmbientesEntreguesByAtividade(id: string): Observable<Ambiente[]> {
        return this.httpClient.get<Ambiente[]>(environment.HOST + this.PATH + '/find-atividades-entregues/' + id);
    }


}
