import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private PATH = '/usuario';
  constructor(private httpClient: HttpClient) { }

  public findAll(): Observable<Usuario[]> {
    return this.httpClient.get< Usuario[] >(environment.HOST + this.PATH);
  }
}
