import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTabGroup } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  @ViewChild('tabsBia', { static: false }) tabsBia: MatTabGroup;

  public atividadeSelecionada: any;

  onMudouValor(event) {
    this.tabsBia.selectedIndex = 2;
    this.atividadeSelecionada = event;
  }
}
