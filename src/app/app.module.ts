import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatTabsModule, MatInputModule,
  MatToolbar, MatToolbarModule, MatIcon, MatIconModule,
  MatCardModule, MatDatepickerModule, MatNativeDateModule, MAT_DATE_LOCALE, MatSnackBarModule
} from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { MatTableModule } from '@angular/material';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SistemaComponent } from './sistema/sistema.component';
import { AtividadeComponent } from './atividade/atividade.component';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material';
import { ModuloComponent } from './modulo/modulo.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { GeralComponent } from './atividade/geral/geral.component';
import { RelacaoEntregaComponent } from './atividade/relacao-entrega/relacao-entrega.component';
import { CreateAtividadeComponent } from './atividade/create-atividade/create-atividade.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NovaEntregaComponent } from './atividade/nova-entrega/nova-entrega.component';
import { DialogComponent } from './dialog/dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { AmbienteModuloComponent } from './ambiente-modulo/ambiente-modulo.component';
import { DeletarEntregaComponent } from './atividade/deletar-entrega/deletar-entrega.component';



@NgModule({
  declarations: [
    AppComponent,
    SistemaComponent,
    AtividadeComponent,
    ModuloComponent,
    GeralComponent,
    RelacaoEntregaComponent,
    CreateAtividadeComponent,
    NovaEntregaComponent,
    DialogComponent,
    AmbienteModuloComponent,
    DeletarEntregaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTabsModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    FlexLayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatTableModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    MatDialogModule
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'pt-BR' }],
  entryComponents: [NovaEntregaComponent,DeletarEntregaComponent],


  bootstrap: [AppComponent]
})
export class AppModule { }
