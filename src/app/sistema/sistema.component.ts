import { Component, OnInit, ViewChild } from '@angular/core';
import { SistemaService } from '../services/sistema.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Sistema} from '../models/sistema';


@Component({
  selector: 'app-sistema',
  templateUrl: './sistema.component.html',
  styleUrls: ['./sistema.component.css']
})
export class SistemaComponent implements OnInit {

  @ViewChild('sistemaForm', null)
  public sistemaForm: FormGroup;

  public sistemas: Sistema[];

  public sistema: string;
  public sistemaSave: Sistema;


  constructor(private service: SistemaService) { }

  ngOnInit() {
    this.createForm();
    this.service.findAll().subscribe(res =>
      this.sistemas = res
    );
  }

  createForm() {
    this.sistemaForm = new FormGroup({
      id: new FormControl('', []),
      descricao: new FormControl('', [])
    });
  }

  eventSelection(event) {
    if (event) {
      this.sistemaForm.controls.descricao.setValue(event.descricao);
      this.sistemaForm.controls.id.setValue(event.id);
    } else {
      this.sistemaForm.reset();
    }
  }

  add(form) {
    this.service.save(form.value).subscribe(res =>
      this.sistemas.push(res)
    );
  }

}
