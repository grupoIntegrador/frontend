import { Component, OnInit } from '@angular/core';
import { AtividadeResumed } from 'src/app/models/atividadeResumed';
import { AtividadeService } from 'src/app/services/atividade.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-relacao-entrega',
  templateUrl: './relacao-entrega.component.html',
  styleUrls: ['./relacao-entrega.component.css']
})
export class RelacaoEntregaComponent implements OnInit {

  public atividadeResumedList: AtividadeResumed[];

  displayedColumns: string[] = ['Atividade', 'Descrição', 'Scripts', 'Fontes', 'Data Entrega', 'Ambiente'];
  public dataSource;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private service: AtividadeService) { }

  ngOnInit() {
    this.service.entregues().subscribe(res => {
      this.atividadeResumedList = res;
      this.dataSource = new MatTableDataSource(this.atividadeResumedList);

    });
  }

}
