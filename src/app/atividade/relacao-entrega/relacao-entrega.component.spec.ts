import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelacaoEntregaComponent } from './relacao-entrega.component';

describe('RelacaoEntregaComponent', () => {
  let component: RelacaoEntregaComponent;
  let fixture: ComponentFixture<RelacaoEntregaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelacaoEntregaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelacaoEntregaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
