import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletarEntregaComponent } from './deletar-entrega.component';

describe('DeletarEntregaComponent', () => {
  let component: DeletarEntregaComponent;
  let fixture: ComponentFixture<DeletarEntregaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeletarEntregaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletarEntregaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
