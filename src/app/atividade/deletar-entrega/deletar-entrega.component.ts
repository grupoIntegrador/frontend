import { Component, OnInit, Inject } from '@angular/core';
import { EntregaAmbiente } from 'src/app/models/entrega-ambiente';
import { Ambiente } from 'src/app/models/ambiente';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { AmbienteService } from 'src/app/services/ambiente.service';
import { Atividade } from 'src/app/models/atividade';
import { EntregaAmbientePK } from 'src/app/models/entrega-ambiente-pk';

@Component({
  selector: 'app-deletar-entrega',
  templateUrl: './deletar-entrega.component.html',
  styleUrls: ['./deletar-entrega.component.css']
})
export class DeletarEntregaComponent implements OnInit {

  entrega: EntregaAmbiente;
  ambientes: Ambiente[];
  ambienteSelecionado: any;
  ambienteToSave: Ambiente;
  deletarEntregaForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DeletarEntregaComponent>,
    public ambienteService: AmbienteService,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public atividade: Atividade) { }

  ngOnInit() {
    this.createForm();
    this.loadAmbientes();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  eventSelectionAmbiente(event) {
    this.ambienteToSave = this.ambientes.find(item => item.id === this.ambienteSelecionado);
  }

  close() {
    this.dialogRef.close();
  }

  createForm() {
    this.deletarEntregaForm = new FormGroup({
      motivo: new FormControl('', [])
    });

  }

  loadAmbientes() {
    this.ambienteService.findAmbientesEntreguesByAtividade(this.atividade.id).subscribe(res => {
      this.ambientes = res;
    });
  }

  retirarEntrega() {

    const entregaPK: EntregaAmbientePK = { ambiente: this.ambienteToSave.id, atividade: this.atividade.id };

    this.entrega = {
      ambiente: this.ambienteToSave,
      atividade: this.atividade,
      dataEntrega: null,
      entregaAmbienteId: entregaPK,
      inAmbiente: false
    };

    this.ambienteService.retirarAmbiente(this.entrega).subscribe(
      data => { this.openSnackBarSucesso('Sucesso ao retirar atividade do ambiente selecionado.'); },
      error => {this.openSnackBarErro('Erro ao retirar atividade do ambiente selecionado.', null); },
      () => { this.dialogRef.close(); }
    );

  }

  openSnackBarSucesso(message: string) {
    this.snackBar.open(message, null, {
      duration: 5 * 1000,
      verticalPosition: 'top',
      panelClass: ['snack-sucesso'],
    });
  }

  openSnackBarErro(message: string, erro: string) {
    this.snackBar.open(message, erro, {
      duration: 5 * 1000,
      verticalPosition: 'top',
      panelClass: ['snack-erro'],
    });
  }

}
