import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { AtividadeService } from '../../services/atividade.service';
import { AtividadeResumed } from '../../models/atividadeResumed';
import { MatTabGroup } from '@angular/material';

@Component({
  selector: 'app-geral',
  templateUrl: './geral.component.html',
  styleUrls: ['./geral.component.css']
})
export class GeralComponent implements OnInit {

  @ViewChild('tabsBia', { static: false }) tabsBia: MatTabGroup;

  @Output() atividadeSelecionada = new EventEmitter();


  public atividadeResumedList: AtividadeResumed[];

  displayedColumns: string[] = ['Atividade', 'Descrição', 'Scripts', 'Fontes', 'Sistema', 'Modulo', 'Status', 'Ambiente'];
  public dataSource;

  selectedRowIndex = -1;


  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private service: AtividadeService) { }

  ngOnInit() {
    this.service.findAllResumed().subscribe(res => {
      this.atividadeResumedList = res;
      this.dataSource = new MatTableDataSource(this.atividadeResumedList);

    });
  }

  highlight(row) {
    this.selectedRowIndex = row.id;
    this.atividadeSelecionada.emit(row);
  }

}
