import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AmbienteService } from '../../services/ambiente.service';
import { Ambiente } from '../../models/ambiente';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EntregaAmbiente } from 'src/app/models/entrega-ambiente';
import { Atividade } from 'src/app/models/atividade';
import { FormGroup, FormControl } from '@angular/forms';
import { EntregaAmbientePK } from 'src/app/models/entrega-ambiente-pk';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-nova-entrega',
  templateUrl: './nova-entrega.component.html',
  styleUrls: ['./nova-entrega.component.css']
})
export class NovaEntregaComponent implements OnInit {

  entrega: EntregaAmbiente;
  ambientes: Ambiente[];
  ambienteSelecionado: any;
  dataEntraga: Date;
  ambienteToSave: Ambiente;
  entregaForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<NovaEntregaComponent>,
    public ambienteService: AmbienteService,
    private snackBar: MatSnackBar,
    @Inject(MAT_DIALOG_DATA) public atividade: Atividade) { }

  ngOnInit() {
    this.createForm();
    this.loadAmbientes();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  eventSelectionAmbiente(event) {
    this.ambienteToSave = this.ambientes.find(item => item.id === this.ambienteSelecionado);
  }

  close() {
    this.dialogRef.close();
  }

  createForm() {
    this.entregaForm = new FormGroup({
      dataEntrega: new FormControl('', [])
    });

  }

  loadAmbientes() {
    this.ambienteService.findAll().subscribe(res => {
      this.ambientes = res;
    });
  }

  novaEntrega() {

    const entregaPK: EntregaAmbientePK = { ambiente: this.ambienteToSave.id, atividade: this.atividade.id };

    this.entrega = {
      ambiente: this.ambienteToSave,
      atividade: this.atividade,
      dataEntrega: this.entregaForm.value.dataEntrega,
      entregaAmbienteId: entregaPK,
      inAmbiente: true
    };

    this.ambienteService.entrega(this.entrega).subscribe(
      data => { this.openSnackBarSucesso('Sucesso ao realizar entrega.'); },
      error => { this.openSnackBarErro('Erro ao realizar entrega.', null); },
      () => { this.dialogRef.close(); }
    );

  }

  openSnackBarSucesso(message: string) {
    this.snackBar.open(message, null, {
      duration: 5 * 1000,
      verticalPosition: 'top',
      panelClass: ['snack-sucesso'],
    });
  }

  openSnackBarErro(message: string, erro: string) {
    this.snackBar.open(message, erro, {
      duration: 5 * 1000,
      verticalPosition: 'top',
      panelClass: ['snack-erro'],
    });
  }

}
