import { Component, OnInit, ViewChild, ViewEncapsulation, Input, OnChanges } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Atividade } from '../../models/atividade';
import { Usuario } from '../../models/usuario';
import { UsuarioService } from '../../services/usuario.service';
import { SistemaService } from '../../services/sistema.service';
import { AtividadeService } from '../../services/atividade.service';
import { Sistema } from 'src/app/models/sistema';
import { Modulo } from 'src/app/models/modulo';
import { ModuloService } from 'src/app/services/modulo.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NovaEntregaComponent } from '../nova-entrega/nova-entrega.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DeletarEntregaComponent } from '../deletar-entrega/deletar-entrega.component';

@Component({
  selector: 'app-create-atividade',
  templateUrl: './create-atividade.component.html',
  styleUrls: ['./create-atividade.component.css'],

})
export class CreateAtividadeComponent implements OnInit, OnChanges {

  @Input() atividadeSelecionada = null;

  @ViewChild('atividadeForm', null)
  public atividadeForm: FormGroup;

  private atividade: Atividade;

  public lideres: Usuario[];

  public lideresTec: Usuario[];

  public usuarios: Usuario[];

  public sistemas: Sistema[];

  public modulos: Modulo[];

  public modulosFiltrados: Modulo[];

  public liderSelecionado;
  public liderTecSelecionado;
  public sistemaSelecionado;
  public moduloSelecionado;

  update = false;

  constructor(
    private usuarioService: UsuarioService,
    private sistemaService: SistemaService,
    private atividadeService: AtividadeService,
    private moduloService: ModuloService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.createForm();
    this.setUsuarios();
    this.setSistemas();
    this.setModulos();

  }

  ngOnChanges() {
    if (this.atividadeSelecionada) {
      this.findAtividade();

    }
  }

  createForm() {
    this.atividadeForm = new FormGroup({
      id: new FormControl('', []),
      descricao: new FormControl('', []),
      nomeAtividade: new FormControl('', []),
      modulo: new FormControl('', []),
      liderTecnico: new FormControl('', []),

      liderProjeto: new FormControl('', []),

      branch: new FormControl('', []),
      fontes: new FormControl('', []),
      scripts: new FormControl('', []),
      observacao: new FormControl('', []),
      statusAtividade: new FormControl('', []),
      release: new FormControl('', []),
    });

  }

  eventSelectionLider(event) {
    if (event) {
      const lider = this.lideres.find(usuario => usuario.id === event);
      this.atividadeForm.controls.liderProjeto.setValue(lider);
    } else {
      this.atividadeForm.controls.liderProjeto.reset();
      this.liderSelecionado = null;
    }
  }

  eventSelectionLiderTec(event) {
    if (event) {
      const liderTec = this.lideresTec.find(usuario => usuario.id === event);
      this.atividadeForm.controls.liderTecnico.setValue(liderTec);
    } else {
      this.atividadeForm.controls.liderTecnico.reset();
      this.liderTecSelecionado = null;
    }
  }

  eventSelectionSistema(event) {
    if (event) {
      this.modulosFiltrados = this.modulos.filter(nod => nod.sistema.id === event);

    } else {

      this.moduloSelecionado = null;
      this.sistemaSelecionado = null;
    }
  }

  eventSelectionModulo(event) {
    if (event) {
      const modulo = this.modulos.find(nod => nod.id === event);
      this.atividadeForm.controls.modulo.setValue(modulo);
      this.sistemaSelecionado = modulo.sistema.id;
    } else {
      this.atividadeForm.controls.modulo.reset();
      this.moduloSelecionado = null;
    }
  }

  setUsuarios() {
    this.usuarioService.findAll().subscribe(res => {
      this.usuarios = res;
      this.lideres = this.usuarios;
      this.lideresTec = this.usuarios;
    });
  }

  setSistemas() {
    this.sistemaService.findAll().subscribe(res => {
      this.sistemas = res;
    });
  }

  setModulos() {
    this.moduloService.findAll().subscribe(res => {
      this.modulos = res;
      this.modulosFiltrados = this.modulos;
    });
  }

  findAtividade() {
    this.atividadeService.findAtividade(this.atividadeSelecionada.id).subscribe(
      res => {
        this.atividade = res;

        this.liderSelecionado = this.atividade.liderProjeto.id;
        this.liderTecSelecionado = this.atividade.liderTecnico.id;
        this.sistemaSelecionado = this.atividade.modulo.sistema.id;
        this.moduloSelecionado = this.atividade.modulo.id;
        this.atividadeForm.patchValue({
          id: this.atividade.id,
          descricao: this.atividade.descricao,
          nomeAtividade: this.atividade.nomeAtividade,
          modulo: this.atividade.modulo,
          branch: this.atividade.branch,
          fontes: this.atividade.fontes,
          scripts: this.atividade.scripts,
          observacao: this.atividade.observacao,
          statusAtividade: this.atividade.statusAtividade,
          liderProjeto: this.atividade.liderProjeto,
          liderTecnico: this.atividade.liderTecnico,
          release: (this.atividade.release ? new Date(this.atividade.release) : '')
        });

      }
    );
  }

  salvar() {
    if (this.atividadeForm.value.id) {
      this.atividadeService.update(this.atividadeForm.value).subscribe(
        data => this.openSnackBarSucesso('Sucesso ao atualizar atividade.'),
        error => this.openSnackBarErro('Erro ao atualizar atividade.', error),
      );
    } else {
      this.atividadeService.save(this.atividadeForm.value).subscribe(
        data => this.openSnackBarSucesso('Sucesso ao salvar atividade.'),
        error => this.openSnackBarErro('Erro ao salvar atividade.', error),
      );
    }
  }

  openSnackBarSucesso(message: string) {
    this.snackBar.open(message, null, {
      duration: 5 * 1000,
      verticalPosition: 'top',
      panelClass: ['snack-sucesso'],
    });
  }

  openSnackBarErro(message: string, erro: string) {
    this.snackBar.open(message, erro, {
      duration: 5 * 1000,
      verticalPosition: 'top',
      panelClass: ['snack-erro'],
    });
  }

  novaEntrega() {
    if (this.atividadeForm.value.id) {
      this.dialog.open(NovaEntregaComponent, {
        width: '340px', height: '300px', data: this.atividade
      });
    } else {
      this.openSnackBarErro('Nenhuma atividade selecionada', null);
    }
  }
  retirarAmbiente() {
    if (this.atividadeForm.value.id) {
      this.dialog.open(DeletarEntregaComponent, { width: '340px', height: '360px', data: this.atividade });
    } else {
      this.openSnackBarErro('Nenhuma atividade selecionada', null);
    }
  }

  enviarProducao() {
    if (this.atividadeForm.value.id) {
      this.atividadeService.enviarProducao(this.atividadeForm.value.id).subscribe(
        data => this.openSnackBarSucesso('Sucesso ao enviar atividade para produção.'),
        error => this.openSnackBarErro('Erro ao enviar atividade para produção.', error),
      );
    } else {
      this.openSnackBarErro('Nenhuma atividade selecionada', null);
    }
  }

}
