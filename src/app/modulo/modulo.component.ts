import { Component, OnInit } from '@angular/core';
import { ModuloService } from '../services/modulo.service';

@Component({
  selector: 'app-modulo',
  templateUrl: './modulo.component.html',
  styleUrls: ['./modulo.component.css']
})
export class ModuloComponent implements OnInit {

  constructor(private moduloService : ModuloService) { }

  ngOnInit() {

  }

}
