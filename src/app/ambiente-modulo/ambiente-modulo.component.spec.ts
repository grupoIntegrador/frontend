import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmbienteModuloComponent } from './ambiente-modulo.component';

describe('AmbienteModuloComponent', () => {
  let component: AmbienteModuloComponent;
  let fixture: ComponentFixture<AmbienteModuloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmbienteModuloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmbienteModuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
