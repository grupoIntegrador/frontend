import { Usuario } from './usuario';
import { Modulo } from './modulo';

export interface Atividade {

    id: string;
    descricao: string;
    nomeAtividade: string;
    modulo: Modulo;
    liderTecnico: Usuario;
    liderProjeto: Usuario;
    branch: string;
    fontes: string;
    scripts: string;
    observacao: string;
    statusAtividade: string;
    release: Date;
    inProducao: boolean;
}
