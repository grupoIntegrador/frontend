export interface AtividadeResumed {

    id: string;
    statusAtividade: string;
    ambientes: string;
    descricao: string;
    fontes: string;
    scripts: true;
    nomeAtividade: string;
    modulo: string;
    dataEntrega: string;
    sistema: string;
  }
