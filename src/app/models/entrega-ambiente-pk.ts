import { Ambiente } from './ambiente';
import { Atividade } from './atividade';

export interface EntregaAmbientePK {
  atividade: string;
  ambiente: string;
}