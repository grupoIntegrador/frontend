import { Sistema } from './sistema';

export interface Modulo {
        id: string;
        descricao: string;
        sistema: Sistema;

}
