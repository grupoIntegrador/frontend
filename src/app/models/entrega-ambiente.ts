import { Ambiente } from './ambiente';
import { Atividade } from './atividade';
import { EntregaAmbientePK } from './entrega-ambiente-pk';


export interface EntregaAmbiente {
  atividade: Atividade;
  ambiente: Ambiente;
  entregaAmbienteId: EntregaAmbientePK;
  dataEntrega: Date;
  inAmbiente: boolean;
}
